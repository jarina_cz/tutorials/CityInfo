﻿using CityInfo.API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CityInfo.API
{
    public class CitiesDataStore
    {
        public static CitiesDataStore Current { get; } = new CitiesDataStore();

        public List<CityDto> Cities { get; set; }

        public CitiesDataStore()
        {
            Cities = new List<CityDto>()
            {
                new CityDto()
                {
                    Id = 1,
                    Name = "Kosice",
                    Description = "The one with that cathedral.",
                    PointsOfInterest = new List<PointOfInterestDto>()
                    {
                        new PointOfInterestDto()
                        {
                            Id = 1,
                            Name = "Central Park",
                            Description = "The most visited urban park in the East side of Slovakia."
                        },
                        new PointOfInterestDto()
                        {
                            Id = 2,
                            Name = "Lunix IX",
                            Description = "High population of primitive apes."
                        },
                    }
                },
                new CityDto()
                {
                    Id = 2,
                    Name = "Ostrava",
                    Description = "The one with amazing minig history.",
                    PointsOfInterest = new List<PointOfInterestDto>()
                    {
                        new PointOfInterestDto()
                        {
                            Id = 1,
                            Name = "Vitkovice",
                            Description = "Interesting mines."
                        },
                        new PointOfInterestDto()
                        {
                            Id = 2,
                            Name = "Karolina",
                            Description = "Looks just like Fukusima."
                        },
                    }
                },
                new CityDto()
                {
                    Id = 3,
                    Name = "Prague",
                    Description = "The city with \"100 roofs\".",
                    PointsOfInterest = new List<PointOfInterestDto>()
                    {
                        new PointOfInterestDto()
                        {
                            Id = 1,
                            Name = "St. Vit Cathedral",
                            Description = "most beautiful cathedral in Central Europe."
                        },
                        new PointOfInterestDto()
                        {
                            Id = 2,
                            Name = "Charles Bridge",
                            Description = "Most visited bridge in Central Europe."
                        },
                    }
                }
            };
        }
    }
}
