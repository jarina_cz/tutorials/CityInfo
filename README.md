# CityInfo ASP.NET Core 2.0 App

Application written during the Pluralsight's [Building Your First API with ASP.NET Core](https://app.pluralsight.com/library/courses/asp-dotnet-core-api-building-first) course by Kevin Dockx

## How to follow

* commit was created after every section of course with descriptive names, so it should be easy to follow what was done and why. Some hidden details (like dotnet cli uses) will be written in the section bellow.

## Additional stuff done

## Technologies learned during this course
* [C#](https://en.wikipedia.org/wiki/C_Sharp_(programming_language))
* [.NET Core 2.1.*](https://www.microsoft.com/net)
* [ASP.NET Core 2.1.*](https://www.asp.net/)
* [ASP.NET MVC](https://www.asp.net/mvc)
* [Entity Framework Core 2.1.*](https://docs.microsoft.com/en-us/ef/core/)
